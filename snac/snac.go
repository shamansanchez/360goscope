package snac

import (
	"bytes"
	"encoding/binary"
	"io/ioutil"
)

type Header struct {
	Family    uint16
	Subtype   uint16
	Flags     uint16
	RequestID uint32
}

type RawSNAC struct {
	Header  Header
	Payload []byte
}

type SNAC interface {
	GetSNAC() RawSNAC
}

func (r RawSNAC) GetSNAC() RawSNAC {
	return r
}

func Read(b []byte) SNAC {
	header := Header{}
	input := bytes.NewReader(b)
	binary.Read(input, binary.BigEndian, &header)

	buf, _ := ioutil.ReadAll(input)

	return RawSNAC{
		Header:  header,
		Payload: buf,
	}
}
