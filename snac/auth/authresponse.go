package auth

import (
	"bytes"
	"encoding/binary"

	"gitlab.com/shamansanchez/360goscope/snac"
)

type AuthResponse struct {
	length  uint16
	AuthKey string
}

func (a AuthResponse) GetSNAC() snac.RawSNAC {
	ret := snac.RawSNAC{}

	ret.Header.Family = 0x17
	ret.Header.Subtype = 0x07

	a.length = uint16(len(a.AuthKey))

	buf := &bytes.Buffer{}

	binary.Write(buf, binary.BigEndian, a.length)
	buf.Write([]byte(a.AuthKey))

	ret.Payload = buf.Bytes()

	return ret
}
