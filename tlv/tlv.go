package tlv

import (
	"bytes"
	"encoding/binary"
	"io"

	"gitlab.com/shamansanchez/360goscope/snac"
)

type TLV struct {
	Type   uint16
	Length uint16
	Data   []byte
}

type SNAC struct {
	Family  uint16
	Subtype uint16
	TLVs    []TLV
}

func (s *SNAC) AddTLV(tlvType uint16, data []byte) {
	s.TLVs = append(s.TLVs, BuildTLV(tlvType, data))
}

func (s SNAC) GetSNAC() snac.RawSNAC {
	ret := snac.RawSNAC{}

	ret.Header.Family = s.Family
	ret.Header.Subtype = s.Subtype

	buf := &bytes.Buffer{}

	for _, tlv := range s.TLVs {
		buf.Write(tlv.toBytes())
	}

	ret.Payload = buf.Bytes()

	return ret
}

func BuildTLV(tlvType uint16, data []byte) TLV {
	return TLV{
		Type:   tlvType,
		Length: uint16(len(data)),
		Data:   data,
	}
}

func ParseTLVs(data []byte) []TLV {
	ret := make([]TLV, 0)
	input := bytes.NewReader(data)

	for {

		var tlvType uint16
		var length uint16

		err := binary.Read(input, binary.BigEndian, &tlvType)
		if err == io.EOF {
			break
		} else if err != nil {
			return nil
		}
		binary.Read(input, binary.BigEndian, &length)

		outputData := make([]byte, length)

		binary.Read(input, binary.BigEndian, &outputData)

		ret = append(ret, TLV{
			Type:   tlvType,
			Length: length,
			Data:   outputData,
		})
	}

	return ret

}

func (t TLV) toBytes() []byte {
	buf := &bytes.Buffer{}

	binary.Write(buf, binary.BigEndian, t.Type)
	binary.Write(buf, binary.BigEndian, t.Length)
	buf.Write(t.Data)

	return buf.Bytes()
}
