package main

import (
	"fmt"
	"log"
	"net"

	"gitlab.com/shamansanchez/360goscope/flap"
	"gitlab.com/shamansanchez/360goscope/snac"
	"gitlab.com/shamansanchez/360goscope/snac/auth"
	"gitlab.com/shamansanchez/360goscope/tlv"
)

func main() {
	srv, err := net.Listen("tcp4", "0.0.0.0:5190")

	if err != nil {
		log.Fatal(err)
	}

	defer srv.Close()

	for {
		c, err := srv.Accept()
		if err != nil {
			log.Fatal(err)
		}

		go handleConnection(c)

	}
}

func handleConnection(c net.Conn) {
	fmt.Println(c)
	defer c.Close()

	var seqID uint16
	flap.Write(c, 0x01, &seqID, []byte{0x00, 0x00, 0x00, 0x01})

	for {
		flapData, err := flap.Read(c)

		if err != nil {
			fmt.Println(err)
			return
		}

		log.Printf("%x %x\n", flapData.Header, flapData.Payload)

		log.Println(flapData.Header.Channel)

		if flapData.Header.Channel == 0x02 {
			log.Printf("SNACtime")
			snacData := snac.Read(flapData.Payload)
			rawSnac := snacData.GetSNAC()

			switch {

			case rawSnac.Header.Family == 0x17 && rawSnac.Header.Subtype == 0x06:
				log.Println("wat6")
				log.Println(tlv.ParseTLVs(snacData.GetSNAC().Payload))
				flap.WriteSNAC(c, 0x02, &seqID, auth.AuthResponse{
					AuthKey: "JASON",
				})

			case rawSnac.Header.Family == 0x17 && rawSnac.Header.Subtype == 0x02:
				log.Println("wat2")
				log.Println(tlv.ParseTLVs(snacData.GetSNAC().Payload))

				response := tlv.SNAC{
					Family:  0x17,
					Subtype: 0x03,
				}

				// response.AddTLV(0x08, []byte{0x00, 0x01})
				// response.AddTLV(0x01, []byte("jasonwashere"))
				// response.AddTLV(0x04, []byte("https://2spooky.net"))

				response.AddTLV(0x01, []byte("jasonwashere"))
				response.AddTLV(0x05, []byte("10.0.23.6:5190"))
				response.AddTLV(0x06, []byte("cisforcookie"))
				response.AddTLV(0x11, []byte("https://2spooky.net"))

				log.Println(response)

				flap.WriteSNAC(c, 0x02, &seqID, response)

			}
		}
	}
}
