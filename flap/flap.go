package flap

import (
	"bytes"
	"encoding/binary"
	"fmt"
	"log"
	"net"

	"gitlab.com/shamansanchez/360goscope/snac"
)

// Header is header
type Header struct {
	ID       byte
	Channel  byte
	SeqNum   uint16
	DataSize uint16
}

type FLAP struct {
	Header  Header
	Payload []byte
}

// Read reads
func Read(c net.Conn) (FLAP, error) {
	header := Header{}
	err := binary.Read(c, binary.BigEndian, &header)

	ret := FLAP{
		Header: header,
	}

	if err != nil {
		return ret, err
	}

	buf := make([]byte, header.DataSize)
	readBytes, err := c.Read(buf)
	if err != nil {
		return ret, err
	}

	if uint16(readBytes) != header.DataSize {
		return ret, fmt.Errorf("something horrible happened: %d != %d", uint16(readBytes), header.DataSize)
	}

	ret.Payload = buf

	log.Printf("FLAP %x %x %d %d", header.ID, header.Channel, header.SeqNum, header.DataSize)

	return ret, nil
}

func Write(c net.Conn, channel byte, seqNum *uint16, payload []byte) error {
	header := Header{
		ID:       0x2a,
		Channel:  channel,
		SeqNum:   *seqNum,
		DataSize: uint16(len(payload)),
	}

	binary.Write(c, binary.BigEndian, header)
	c.Write(payload)

	*seqNum++
	return nil
}

func WriteSNAC(c net.Conn, channel byte, seqNum *uint16, payload snac.SNAC) error {
	snac := payload.GetSNAC()

	output := bytes.NewBuffer([]byte{})

	binary.Write(output, binary.BigEndian, snac.Header)
	output.Write(snac.Payload)

	header := Header{
		ID:       0x2a,
		Channel:  channel,
		SeqNum:   *seqNum,
		DataSize: uint16(len(output.Bytes())),
	}

	binary.Write(c, binary.BigEndian, header)
	c.Write(output.Bytes())

	*seqNum++
	return nil
}
